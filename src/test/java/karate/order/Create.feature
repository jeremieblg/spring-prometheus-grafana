
Feature: Create Order ...

  Background:
    * url baseUrl
    * def orderBase = '/orders/'

  Scenario: Create order

    Given path orderBase
    And request {name: "Casque",client: { firstname: "Paul", lastname: "Michot", address: "Brest"}}
    And header Accept = 'application/json'
    When method post
    Then status 200
    And match response == 'abcaae56f23312187b4d662ad3397fa5cf44d0bfc61a0d95046f68600494ec10'